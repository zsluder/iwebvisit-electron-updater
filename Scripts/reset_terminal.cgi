#!/perl/bin/perl

# That script kills then resets IE, but ONLY if the terminal can http-ping the server -- incoming gives the location of http test page 
# used to hit a server in the cloud before terminals starting hitting the facility server instead

use Win32::OLE qw( in );
use Win32::OLE::Variant;

use Win32::Process;

use CGI qw/:standard/;
CGI::initialize_globals();

$cloud_ping_script = param('cloud_addr'); #full http address of file to ping in cloud
$fac_token = param('fac_token');

use LWP::UserAgent;
$ua = LWP::UserAgent->new;
$ua->timeout( 3 ); # seconds

$res = $ua->get($cloud_ping_script);
#print $res->decoded_content."\n";

  
if( $res->is_success ) {
  #$terminal_ip = &get_terminal_ip;
  #$url = "http://www.iwebvisit.com/admin/admin/admin_housing_units.php?task=load_for_terminal&fac_token=$fac_token&terminal_ip=$terminal_ip";
  
  $url = "http://localhost/end_of_visit.cgi";
  
  &kill_ie;
  sleep(3);
  &launch_ie($url); 
  
  print "Content-type: text/plain\n\n";
  print "true";
}
else {
   print "Content-type: text/plain\n\n";
   print "false";
}
#exit; Yikes.  Exits the Perl HTTP server too.



#######################################################################################
sub launch_ie {
    #########################################################
    # code to launch IE to the mm url
    #########################################################
    
    $b = Win32::OLE->new("InternetExplorer.Application");
      
    $b -> {'Visible'} = 1;
    $b -> {'ToolBar'} = 1;
    $b -> {'StatusBar'} = 0;
    $b -> {'Width'} = 1440;
    $b -> {'Height'} = 900;	
    #$b -> {'FullScreen'} = 1;	# Yikes -- don't even have the x to minimize the window any more
    #$b -> {'TheaterMode'} = 1;  # seems to be the same at FullScreen
    
    $b -> {'Left'} = 0;
    $b -> {'Top'} = 0;
    #sleep(1);
    $b -> Navigate ($_[0]);
      
    #print "Content-type: text/plain\n\n";   
    #print 'Terminal Activated!';
    #print "\n";

}


#######################################################################################
sub kill_ie {
    #########################################################
    # code to kill any live IE process
    #########################################################
    
    $Machine = "\\\\.";
    $Machine = shift @ARGV if( $ARGV[0] =~ /^\\\\/ );
    
    # WMI Win32_Process class
    $CLASS = "winmgmts:{impersonationLevel=impersonate}$Machine\\Root\\cimv2";
    $WMI = Win32::OLE->GetObject( $CLASS ) || die;
    
    # getting an instance of win32 obects -- Win32_Process in this case
    #http://msdn.microsoft.com/en-us/library/aa394507%28v=VS.85%29.aspx
    foreach my $Proc ( sort {lc $a->{Name} cmp lc $b->{Name}} in( $WMI->InstancesOf( "Win32_Process" ) ) )
    {
      
      if ( $Proc->{'Name'} eq 'iexplore.exe' ) {
        $ie_process_id = $Proc->{'ProcessID'};
      }
    
      #printf( "% 5d) %s ", $Proc->{ProcessID}, "\u$Proc->{Name}" );
      #print "( $Proc->{ExecutablePath} )" if( "" ne $Proc->{ExecutablePath} );
      #print "\n";
      
      Win32::Process::KillProcess($ie_process_id, '');
    }

}


###################################################
sub get_terminal_ip {
    my @data = `ipconfig`;
    foreach $d(@data) {
        #print '-'.$d."<br>";
        if ($d =~ /IP Address/ || $d =~ /IPv4 Address/) {
            # IP Address works on XP, IPv4 Address on Win7
            my @parts = split(/:/,$d);
            return trim(@parts[1]);
        }
    }
}

###################################################
sub trim($){
	my $string = shift;
	$string =~ s/^\s+//;
	$string =~ s/\s+$//;
	return $string;
}