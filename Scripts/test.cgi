#!/perl/bin/perl

# or  !C:\Perl\bin\perl.exe

$terminal_ip = &get_terminal_ip;

print "Content-type: text/plain\n\n";
print 'Hello, my IP is:';
print "\n";
print $terminal_ip;




###################################################
sub get_terminal_ip {
    my @data = `ipconfig`;
    foreach $d(@data) {
        #print '-'.$d."<br>";
        if ($d =~ /IP Address/ || $d =~ /IPv4 Address/) {
            # IP Address works on XP, IPv4 Address on Win7
            my @parts = split(/:/,$d);
            return trim(@parts[1]);
        }
    }
}

###################################################
sub trim($){
	my $string = shift;
	$string =~ s/^\s+//;
	$string =~ s/\s+$//;
	return $string;
}
