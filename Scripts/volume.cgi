#!/perl/bin/perl

# This script issues a system command to get the NIRcmd utility to change the system volume

use CGI qw/:standard/;
CGI::initialize_globals();

$new_volume = param('new_volume');

$res = system ('C:\nircmd\nircmd.exe setsysvolume ' . $new_volume );

print "Content-type: text/plain\n\n";

if ($res==0) {
  print "true";
}
else {
  print "false";
}


