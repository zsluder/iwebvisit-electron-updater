#!/perl/bin/perl

# This script kills then resets Chrome, but ONLY if the terminal can http-ping the facility server

use Win32::OLE qw( in );
use Win32::OLE::Variant;

use Win32::Process;

use CGI qw/:standard/;
CGI::initialize_globals();

$ping_script = param('cloud_addr'); #used to be server in cloud -- now facility server
$fac_token = param('fac_token');

use LWP::UserAgent;
$ua = LWP::UserAgent->new;
$ua->timeout( 3 ); # seconds

$res = $ua->get($ping_script);
#print $res->decoded_content."\n";

if( $res->is_success ) {
  print "Content-type: text/plain\n\n";
  print "true";
  
  $ua2 = LWP::UserAgent->new;
  $ua2->timeout( 10 ); # seconds
  $end_visit_script = 'http://localhost/visit.cgi?task=stop'; 
  $res = $ua2->get($end_visit_script);
}
else {
   print "Content-type: text/plain\n\n";
   print "false";
}
#exit; Yikes.  Exits the Perl HTTP server too. 
