#!/perl/bin/perl

print "Content-type: text/html\n\n";
print <<THEFILE;

  <!DOCTYPE html>    <html>      <head>        <title>Terminal Reset</title>      </head>
      <body>          <h2>Video Terminal Reset in Progress . . . . . . . . .</h2>      </body>    </html>THEFILE

$end_visit_script = 'http://localhost/visit.cgi?task=stop';

use LWP::UserAgent;
$ua = LWP::UserAgent->new;
$ua->timeout( 10 ); # seconds

$res = $ua->get($end_visit_script);

#print $res->decoded_content."\n";
#exit; # yikes -- will kill the perl http server too
