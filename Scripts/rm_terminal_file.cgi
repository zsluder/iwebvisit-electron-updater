#!/perl/bin/perl

# Example -- need full path from $base_dir
# http://10.211.55.4/rm_terminal_file.cgi?file_names_only=yes&files=html\6.8-iwvTerminalVersion.txt

use CGI qw/:standard/;
CGI::initialize_globals();

@files_to_remove = split(/,/,param('files')); 

$base_dir = "C:\\iwvTerminal\\"; 

print "Content-type: text/plain\n\n";

if ( $#files_to_remove < 0 ) {
   print "----------------------------------------------------------------------------------------------\n";
   print "Invalid Terminal Update:  No Files Specified.\n";
   print "----------------------------------------------------------------------------------------------\n";
}
else {
    foreach $file (@files_to_remove) {
        
        $full_path = $base_dir.$file;
        if( unlink($full_path) == 1 ) {
          
          print "----------------------------------------------------------------------------------------------\n";
          print "$full_path was deleted\n";
          print "----------------------------------------------------------------------------------------------\n";
        }
        else {
          print "----------------------------------------------------------------------------------------------\n";
          print "No Dice Amigo: $full_path was not deleted.\n";
          print "----------------------------------------------------------------------------------------------\n";
        }
    }
}