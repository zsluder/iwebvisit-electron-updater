#!/perl/bin/perl

# Grabs the MAC address from off a terminal and return it

print "Content-type: text/plain\n\n";

$mac = &get_mac;
print $mac;

###################################################
sub get_mac {
    my @data = `getmac`;
    
    foreach $d(@data) {
        #print '-'.$d."\n";
        if ($d =~ /Device/ ) {
           #print '-'.$d."\n";
           my @parts = split(/ /,$d);
           return $parts[0];
        }
    }
}

###################################################