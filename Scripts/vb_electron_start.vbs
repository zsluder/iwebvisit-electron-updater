Dim objShellDIM fso

Set objShell = WScript.CreateObject( "WScript.Shell" )
Set fso = CreateObject("Scripting.FileSystemObject")

URL = Wscript.Arguments(0)

If (fso.FileExists("C:\iwvTerminal\Scripts\iwebvisit_terminal\iwebvisit_terminal.exe")) Then
  objShell.Run """C:\iwvTerminal\Scripts\iwebvisit_terminal\iwebvisit_terminal.exe""  " & URL & " --start-fullscreen"
Else
  objShell.Run """C:\Program Files\Google\Chrome\Application\chrome.exe""  " & " --start-maximized " & URL
End If

'objShell.Run """C:\Program Files\Google\Chrome\Application\chrome.exe"" " & " --incognito " & URL
'--incognito gets rid of the didn't shut down properly notice, but messes with the ability to auto accept camera without human intervention

Set objShell = Nothing
