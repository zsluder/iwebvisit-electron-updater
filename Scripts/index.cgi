#!/perl/bin/perl

use Win32::OLE qw( in );
use Win32::OLE::Variant;

use Win32::Process;

use CGI qw/:standard/;
CGI::initialize_globals();


$task = param('task');
if( $task eq 'open' ) {
        #########################################################
        # must be within this number of minutes before visit is to begin
        # or else this script won't launch the browser
        
        $minutes_before_launch = 15;
        
        #########################################################
        
        # submitted date of form -- 2010-10-12 02:30:00 UTC time
        $date_time = param('date_time');
        @parts = split(/ /, $date_time);
        @dates = split(/-/, @parts[0]);
        @times = split(/:/, @parts[1]);
        
        $year = $dates[0];
        $month = $dates[1];
        $day = $dates[2];
        
        $hour = $times[0];
        $min = $times[1];
        $sec = $times[2];
        
        use DateTime;
        
        $tz = DateTime::TimeZone->new( name => 'UTC' );
        $dt = DateTime->new( year   => $year,
                             month  => $month,
                             day    => $day,
                             hour   => $hour,
                             minute => $min,
                             second => $sec,
                             nanosecond => 0,
                             time_zone => $tz,
                           );
        $visit_timestamp = $dt->epoch();
        
        # UTC since epoch as determined from local machine
        $now_timestamp = time();
        
        $minutes_till_meeting = ($visit_timestamp - $now_timestamp)/60;
        # will be negative if past meeting time
        
        if ( $minutes_till_meeting > $minutes_before_launch ) {
            print "Content-type: text/plain\n\n";   
            print 'Terminal Not Activated!';
            print "\n";
            print 'Minutes Remaining Before Visit: ' . $minutes_till_meeting ;
            print "\n";
        }
        else {
          &kill_ie;
          sleep(3);
          &launch_ie;
        }
} # end task open

elsif($task eq 'quit_for_next_visit') {
      &kill_ie;
      sleep(3);
      &launch_ie; ## here the url will be our notification page
}


#######################################################################################
sub launch_ie {
    #########################################################
    # code to launch IE to the mm url
    #########################################################
    
    $b = Win32::OLE->new("InternetExplorer.Application");
      
    $b -> {'Visible'} = 1;
    $b -> {'ToolBar'} = 1;
    $b -> {'StatusBar'} = 0;
    $b -> {'Width'} = 1440;
    $b -> {'Height'} = 900;	
    #$b -> {'FullScreen'} = 1;	# Yikes -- don't even have the x to minimize the window any more
    #$b -> {'TheaterMode'} = 1;  # seems to be the same at FullScreen
    
    $b -> {'Left'} = 0;
    $b -> {'Top'} = 0;
    #sleep(1);
    $b -> Navigate (param('url'));
      
    print "Content-type: text/plain\n\n";   
    print 'Terminal Activated!';
    print "\n";

}


#######################################################################################
sub kill_ie {
    #########################################################
    # code to kill any live IE process
    #########################################################
    
    $Machine = "\\\\.";
    $Machine = shift @ARGV if( $ARGV[0] =~ /^\\\\/ );
    
    # WMI Win32_Process class
    $CLASS = "winmgmts:{impersonationLevel=impersonate}$Machine\\Root\\cimv2";
    $WMI = Win32::OLE->GetObject( $CLASS ) || die;
    
    # getting an instance of win32 obects -- Win32_Process in this case
    #http://msdn.microsoft.com/en-us/library/aa394507%28v=VS.85%29.aspx
    foreach my $Proc ( sort {lc $a->{Name} cmp lc $b->{Name}} in( $WMI->InstancesOf( "Win32_Process" ) ) )
    {
      
      if ( $Proc->{'Name'} eq 'iexplore.exe' ) {
        $ie_process_id = $Proc->{'ProcessID'};
      }
    
      #printf( "% 5d) %s ", $Proc->{ProcessID}, "\u$Proc->{Name}" );
      #print "( $Proc->{ExecutablePath} )" if( "" ne $Proc->{ExecutablePath} );
      #print "\n";
      
      Win32::Process::KillProcess($ie_process_id, '');
    }

}
