#!/perl/bin/perl

use Win32::OLE qw( in );

use Win32::OLE::Variant;

use Win32::Process;

use CGI qw/:standard/;


CGI::initialize_globals();


$timestamp = time();


#################################################

# Per-facility Defines

#################################################

use lib 'c:\iwvTerminal\Scripts\\';

use CONFIG;


#$fac_token = CONFIG::get_fac_token();

$launcher_ip = CONFIG::get_launcher_ip();

#################################################

print "Content-type: text/plain\n\n";

$task = param('task');


if($task eq 'start') {
      if ( defined(param('patch_volume')) ) {

          # it should always be defined, but nircmd will throw an alert window if the command below has no arguent, so this is just a precaution

          system ('C:\nircmd\nircmd.exe setsysvolume ' . param('patch_volume') );
      }

     &kill_app('iwebvisit_terminal.exe');

     sleep(2);

     &launch_app(param('url'));

     print "launched";

} elsif ($task eq 'stop') {

      &kill_app('iwebvisit_terminal.exe');

      sleep(2);

      $terminal_ip = &get_terminal_ip;

      &launch_app("http://$launcher_ip/php/terminal/index.php?terminal_ip=$terminal_ip");

} else {
      print "Ready For Action\n";
}


#######################################################################################

sub kill_app {
     $app_name = $_[0];

     $Machine = "\\\\.";

     $Machine = shift @ARGV if( $ARGV[0] =~ /^\\\\/ );

     # WMI Win32_Process class

     $CLASS = "winmgmts:{impersonationLevel=impersonate}$Machine\\Root\\cimv2";

     $WMI = Win32::OLE->GetObject( $CLASS ) || die;

    # getting an instance of win32 obects -- Win32_Process in this case
    #http://msdn.microsoft.com/en-us/library/aa394507%28v=VS.85%29.aspx

     foreach my $Proc ( sort {lc $a->{Name} cmp lc $b->{Name}} in( $WMI->InstancesOf( "Win32_Process" ))) {
          if ($Proc->{'Name'} eq $app_name) {
                $process_id = $Proc->{'ProcessID'};
          }

          Win32::Process::KillProcess($process_id, '');
    }

}

#######################################################################################

sub launch_app {
    system ('C:\iwvTerminal\Scripts\vb_electron_start.vbs',$_[0]);
}

#######################################################################################

sub get_terminal_ip {
      my @data = `ipconfig`;

      foreach $d(@data) {
            if ($d =~ /IP Address/ || $d =~ /IPv4 Address/) {

                # IP Address works on XP, IPv4 Address on Win7
                my @parts = split(/:/,$d);

                return trim(@parts[1]);
            }
      }

}

#######################################################################################

sub trim($){
      my $string = shift;

      $string =~ s/^\s+//;

      $string =~ s/\s+$//;

      return $string;
}
