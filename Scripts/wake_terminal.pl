#!/perl/bin/perl

# A script that http-pings the cloud -- This was the original setup, but eventually we needed to stop terminals from hitting our cloud servers as they multiplied.
# Just to force a terminal to wake itself, it is sufficient to make a request to itself over port 80 -- Basically, it pinches itself

# when pinching themselves, this doesn't matter any more
#so we can set up the cron for all terminals at the same time, but they won't all fire at the same time
#$max = 30;
#$random_seconds = int(rand($max));
#sleep($random_seconds);

use File::Copy;
use DateTime;

#$cloud_ping_script = 'http://174.143.208.217/cloud_test.php'; #full http address of file to ping in cloud
$ping_script = 'http://localhost/test.cgi';

$before = time();

use LWP::UserAgent;
$ua = LWP::UserAgent->new;
$ua->timeout( 30 ); # seconds

$res = $ua->get($ping_script);
#print $res->decoded_content."\n";

#when pinching itself, this probably isn't necessary any more, but no harm in leaving it
$total_time = time() - $before;

if( $res->is_success ) {
  $log_entry = "SUCCESS ($total_time sec)";
}
else {
  $log_entry = "FAILURE ($total_time sec)";
}

my ($sec,$min,$hour,$day,$month,$yr19,@rest) = localtime(time);

open(LOGFILE,'>>c:\iwvTerminal\Scripts\wake_log.txt') or die "WTF";
print LOGFILE ($yr19+1900)."-".++$month."-$day ";
print LOGFILE sprintf("%02d",$hour).":".sprintf("%02d",$min).":".sprintf("%02d",$sec);
print LOGFILE "\t$log_entry\n";
close(LOGFILE);

$path_to_logs = 'c:\iwvTerminal\Scripts';
police_log_file($path_to_logs,'\wake_log.txt','\wake_log_copy.txt');


#######################################################################################
sub police_log_file {
  my $log_path       = $_[0];
  my $file_name      = $_[1];
  my $tmp_file_name  = $_[2];
  my $lines_in_file = 0;
  my $lines_processed = 0;

  copy($log_path.$file_name , $log_path.$tmp_file_name) or die "Copy failed: $!";

  open(TMP_LOGFILE,"< $log_path$tmp_file_name") or die "WTF";
  $lines_in_file++ while <TMP_LOGFILE>;
  close(TMP_LOGFILE);

  $keep_after_line = 0;
  if ( $lines_in_file > 1000 ) {
    $keep_after_line = $lines_in_file - 500;

    open(TMP_LOGFILE,"< $log_path$tmp_file_name") or die "WTF";
    open(LOGFILE,"> $log_path$file_name") or die "WTF";

    $lines_processed = 0;
    while ( $line = <TMP_LOGFILE> ) {
      $lines_processed++;
      if ( $lines_processed>=$keep_after_line ) {
        print LOGFILE  $line;
      }
    }

    close(TMP_LOGFILE);
    close(LOGFILE);
  }
}
