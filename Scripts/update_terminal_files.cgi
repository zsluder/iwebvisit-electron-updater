#!/perl/bin/perl

# full call example
# http://10.211.55.4/update_terminal_files.cgi?file_names_only=yes&up_one_dir=yes&files=testing.html

use CGI qw/:standard/;
CGI::initialize_globals();

$file_names_only = param('file_names_only');

@files_to_update = split(/,/,param('files'));

$url = "http://www.iwebvisit.com/terminal_repository/"; # need trailing slash

$base_dir = "C:\\iwvTerminal\\Scripts\\"; 
if ( param('up_one_dir') eq 'yes' ) {
  #this parameter allows us to place files one dir above the terminals web root -- to update p5httpd itself, for example
  $base_dir = "C:\\iwvTerminal\\";
}


use LWP::UserAgent;
$ua = LWP::UserAgent->new;
$ua->timeout( 5 ); # seconds

print "Content-type: text/plain\n\n";

if ( $#files_to_update < 0 ) {
   print "----------------------------------------------------------------------------------------------\n";
   print "Invalid Terminal Update:  No Files Specified.\n";
   print "----------------------------------------------------------------------------------------------\n";
}
else {
    foreach $file (@files_to_update) {

        $res = $ua->get($url.$file);

        if( $res->is_success ) {
          $file_contents = $res->decoded_content;

          $full_path = $base_dir.$file;
          open(FILEHANDLE, ">$full_path");
          print FILEHANDLE $file_contents;
          close(FILEHANDLE);

          if ( $file_names_only eq 'yes') {
            print "----------------------------------------------------------------------------------------------\n";
            print "$file was written. \n";
            print "----------------------------------------------------------------------------------------------\n";
          }
          else {
            print "----------------------------------------------------------------------------------------------\n";
            print "The following was written To file: $file\n";
            print "----------------------------------------------------------------------------------------------\n";
            print $file_contents;
          }

        }
        else {
          print "----------------------------------------------------------------------------------------------\n";
          print "No Dice Amigo: $file was not found in the terminal code repository.\n";
          print "----------------------------------------------------------------------------------------------\n";
        }
    }
}
