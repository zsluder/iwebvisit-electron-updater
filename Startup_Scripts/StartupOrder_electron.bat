@ECHO OFF

ECHO Waiting for startup ...
TIMEOUT /T 90

:: Updates all the terminal files from a Bitbucket repository
ECHO.
ECHO Updating terminal files ...
C:\iwvTerminal\Git_Portable\App\Git\cmd\git config user.email "zsluder@omnispear.com"
C:\iwvTerminal\Git_Portable\App\Git\cmd\git pull https://zsluder@bitbucket.org/zsluder/iwebvisit-electron-updater.git terminal-updater


:: Launches HTTP Server
ECHO.
ECHO Launching HTTP Server ...
START /MIN "" "C:\iwvTerminal\p5httpd.pl"


:: Launches Electron
ECHO.
ECHO Launching Electron Browser ...
CSCRIPT "C:\iwvTerminal\Scripts\vb_electron_start.vbs" http://localhost/end_rtc_visit.cgi


:: Fixes the issue where Electron hangs on 'Visit Terminal Reset in Progress...'
TIMEOUT /T 10
taskkill /F /IM iwebvisit_terminal.exe
