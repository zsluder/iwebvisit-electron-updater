Startup scripts to start the HTTP server and terminal.
NOTE: Older versions were removed for cleanup

StartupOrder.bat          = Original launcher
StartupOrder_ie.bat       = launching IE for the original visitation system
StartupOrder_chrome.bat   = launching Chrome for the 2.0 WebRTC visitation system
StartupOrder_electron.bat = launching Electron Chromium Browser for the 3.0 visitation system

To switch to another startup option, other files referencing electron need to be changed to the desired browser
	- /Startup_Scripts/StartupOrder_electron.bat
	- /Scripts/vb_electron_start.vbs
	- /Scripts/visit.cgi