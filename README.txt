  *************************************************************
 |                          Electron                           |
  *************************************************************
- Electron Chromium Browser is the new browser used for visits
- The only files referencing Electron are:
	- /Startup_Scripts/StartupOrder_electron.bat
	- /Scripts/vb_electron_start.vbs
	- /Scripts/visit.cgi

- Automatic updates are made by pulling a repository on Bitbucket when 'StartupOrder_electron.bat' runs on computer restart
- Use path 'C:/iwvTerminal/Git_Portable/App/Git/cmd/git' to run any git commands

 NOTE: On intitial install updating is a little slow. Terminal files are updated when computer restarts



  *************************************************************
 |                    Only For New Installs                    |
  *************************************************************

1) =============================================================
   Put the 'iwvTerminal' folder on the C drive -- must be exactly 'C:/iwvTerminal'

 
2) =============================================================
   Turn off the firewall which will block external requests

   Go to the Windows Control Panel (Choose 'Switch to Classic View')
a) If there is a "Windows Firewall' control panel, select that and turn off the firewall

b) Otherwise in the 'Control Panel' go to:
   Network Connections -> local area -> right click on properties -> click advanced -> click the 'settings' button under 'Windows Firewall' -> turn it off


3) =============================================================
   Run the Active Perl Installer

   In the screen that comes up, make sure all of the checkboxes are checked so that all features are installed


4) =============================================================
   NOTE: 2013-03
   Got Eror when trying to install the module below. Just use the older Activestate 5.12 where the DBD-mysql driver works.

   Install Perl's DateTime module

a) Open a Command Prompt (Should be under Start -> Command Prompt)

b) Type the following command (stands for Perl Package Manager)
   'ppm'
   
   If this doesn't work launch PPM directly from the start menu.
   As PPM launches -- wait till it downloads the package repository

c) Click on View All Packages button to far left of search pane (or use View menu at top of PPM window)

d) Highlight DateTime --> then Click Mark For Install button immediately to right of search field 

e) Click the Run Marked Actions button (the green arrow)
   Takes a while ... wait till it says DONE in bold
 

5) =============================================================
   Make sure the date and time is set properly on the box.
   Go to: Control Panel --> Date and Time 
a) In the Time Zone tab, make sure the proper time zone is set AND the button is checked to automatically adjust to daylight savings

b) In the Internet Time tab, make sure it is set to Automatically Sync with a time zone server.


6) =============================================================
   Start 'C:/iwvTerminal/p5httpd.pl' server by clicking on it

   IMPORTANT: Before answering yes, be sure to uncheck the checkbox that says display this warning each time you open this program.


7) =============================================================
   Put a shortcut to this file the system startup folder 
   'C:/iwvTerminal/Startup_Scripts/StartupOrder_electron.bat'
   It is included in the iwvTerminal package.  On startup, this first launches the p5httpd server, then Electron a few seconds later. 

   Open 'StartupOrder_electron.bat'
   IMPORTANT: Before answering yes, be sure to uncheck the checkbox that says display this warning each time you open this program.

   For XP: C:\Documents and Settings\USER\Start Menu\Programs\Startup
   Vista: Start Menu -> All Programs -> Startup (right click to open folder)


8) =============================================================
   Make sure the hard drive is set to never sleep, but the monitor can sleep


9) =============================================================
After imaging a bunch of terminals, do step 7 above for each terminal so that the IP addresses are accurate.




  *************************************************************
 |                   Updates to the Terminal                   |
  *************************************************************

1) =============================================================
   Shut down any running instances of the Perl http server 
   Just close the command window with 'C:/Perl/bin/perl.exe' at the top, this might be minimized in the taskbar

   Shut down any running instances of Electron


2) =============================================================
   Delete any existing copies of 'C:/iwvTerminal' folder


3) =============================================================
   Put the 'iwvTerminal' folder on the C drive -- must be exactly 'C:/iwvTerminal'


4) =============================================================
   Click on the 'C:/iwvTerminal/p5httpd.pl' icon to start the web server

   IMPORTANT: Before answering yes, be sure to uncheck the checkbox that says display this warning each time you open this program.




  *************************************************************
 |                          Testing                            |
  *************************************************************

You can do this directly from any web browser that can make requests to the terminal

This is basically the same request that an 'Inmate' button in the dashboard sends to the terminal.
Just paste it into the browser's address field:

NOTE: The time is GMT, so adjust for your local time zone
From Pacific, GMT is +7 or +8 depending on DST

If the date time you submit is more than 15 minutes into the future, then you will get a message to that effect.  
Otherwise, IE will launch and surf to the supplied URL.

http://10.211.55.3/?task=open&minutes=30&url=http://www.cnn.com&date_time=2011-10-18 22:19:00
================================================================

Windows XP Scheduled Reboot Instructions


To help you automate this type of operation, Windows XP comes with a command-line utility called Shutdown.exe, which can restart your system. To make this happen automatically, you can configure it to run at a specified time with the Scheduled Tasks tool. Here's how:

   1. Go to Control Panel | Scheduled Tasks.
   2. Double-click Add Scheduled Task to launch the Scheduled Task Wizard.
   3. Click Next and then click the Browse button.
   4. Access the Windows\System32 folder, select Shutdown.exe, and click Open.
   5. Follow the wizard through the next two screens to give the task a name and choose a schedule.
   6. Enter your user account name and password and click Next.
   7. Select the Open Advanced Properties check box and click Finish.
   8. In the task's Properties dialog box, add the /r parameter to the end of the command line in the Run text box and click OK. (Be sure to include a space between the last character in the command name and the first character in the parameter list.)
   9. Enter your user account name and password and click OK.

When the Shutdown utility runs, you'll momentarily see a small dialog box on your screen before the system restarts.


inmatePatch	String	http://192.168.1.21/?task=open&minutes=0&url=http%3A%2F%2Fnvwashoecountyjail.iwebvisit.com%2Fmeeting%2Findex.html%23id%3D630%26session%3Duvww4c9d3dd8d8f7e%26conn%3Drtmp%26name%3DArthur+Smith